# Dependency Check NIST NVD

[![Maven Setup License](https://img.shields.io/badge/Apache_License-2.0-orange.svg "Maven Setup License")](https://www.apache.org/licenses/LICENSE-2.0.html)
[![Dependency Check NIST NVD pipeline](https://gitlab.com/freedumbytes/dependency-check-nist-nvd/badges/master/build.svg "Dependency Check NIST NVD pipeline")](https://gitlab.com/freedumbytes/dependency-check-nist-nvd)

[![Dependency Check NIST NVD Site](https://freedumbytes.gitlab.io/setup/images/icon/maven-v.png "Dependency Check NIST NVD Site")](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/)
[![Dependency Check NIST NVD Site](https://img.shields.io/badge/javadoc-1.0.0-yellow.svg?color=yellow&label=Dependency%20Check%20NIST%20NVD "Dependency Check NIST NVD Site")](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/)

# National Vulnerability Database

The [National Vulnerability Database](https://nvd.nist.gov/general) (NVD) is the U.S. government repository of standards based vulnerability management data represented using the [Security Content Automation Protocol](https://scap.nist.gov/) (SCAP).
This data enables automation of vulnerability management, security measurement, and compliance.
The NVD includes databases of security checklist references, security related software flaws, misconfigurations, product names, and impact metrics.

Originally created in 2000 (called Internet - Categorization of Attacks Toolkit or ICAT), the NVD has undergone multiple iterations and improvements and will continue to do so to deliver its services.
The NVD is a product of the [National Institute of Standards and Technology](https://www.nist.gov/) (NIST) Computer Security Division, Information Technology Laboratory
and is sponsored by the Department of Homeland Security’s National Cyber Security Division.

The NVD performs analysis on [CVE](https://cve.mitre.org/)s that have been published to the CVE Dictionary.
NVD staff are tasked with analysis of CVEs by aggregating data points from the description, references supplied and any supplemental data that can be found publicly at the time.
This [analysis results](https://nvd.nist.gov/general/nvd-dashboard) in association impact metrics ([Common Vulnerability Scoring System](https://nvd.nist.gov/vuln-metrics/cvss) - CVSS),
[vulnerability types](https://nvd.nist.gov/vuln/categories) ([Common Weakness Enumeration](https://cwe.mitre.org/data/index.html) - CWE),
and applicability statements ([Common Platform Enumeration](https://nvd.nist.gov/products/cpe/statistics) - CPE), as well as other pertinent metadata.
The NVD does not actively perform vulnerability testing, relying on vendors, third party security researchers and vulnerability coordinators to provide information that is then used to assign these attributes.
As additional information becomes available CVSS scores, CWEs, and applicability statements are subject to change.
The NVD endeavors to re-analyze CVEs that have been amended as time and resources allow to ensure that the [information](https://nvd.nist.gov/general/visualizations) offered is [up to date](https://nvd.nist.gov/vuln/full-listing).

## How can my organization use the NVD data within our own products and services?

All NVD data is [freely available](https://nvd.nist.gov/general/faq#1f2488ea-0492-45a7-ae5b-ad29bc31dd05) from our XML Data Feeds. There are no fees, licensing restrictions, or even a requirement to register.
All NIST publications are available in the public domain according to Title 17 of the United States Code.
Acknowledgment of the NVD  when using our information is appreciated. In addition, please email nvd@nist.gov to let us know how the information is being used.

# Performance

When running `mvn org.owasp:dependency-check-maven:aggregate` on GitLab CI the Maven repository always starts out empty and thus all the [XML Vulnerability Feeds](https://nvd.nist.gov/vuln/data-feeds)
for the [National Vulnerability Database](https://nvd.nist.gov) (NVD) must be imported into the H2 Database `dc.h2.db`, that lives in local Maven repository `org/owasp/dependency-check-data/3.0`.

This Dependency Check NIST NVD project contains a couple of H2 Configuration Tests and a Proof Of Concept for a database backup/restore mechanism.

## H2 Fast Database Import

The H2 Database documentation considers using the following options, temporarily, to [speed up large imports](https://www.h2database.com/html/performance.html#fast_import):

Option     | Description                   | Value
:----------|:------------------------------|-----:
LOG        | Disabling the transaction log | 0
CACHE_SIZE | A large cache is faster       | 65536
LOCK_MODE  | Disable locking               | 0
UNDO_LOG   | Disable the session undo log  | 0

Testing the following combinations of those options locally resulted in:

| LOG        | CACHE_SIZE | LOCK_MODE  | UNDO_LOG   | Total  time (sec)
|:----------:|:----------:|:----------:|:----------:|-----------------:
|            |            |            |            | 128
| X          | X          | X          | X          |  56
| X          | X          | -          | X          |  59
| X          | X          | -          |            |  58
| X          |            | -          | X          |  70
| X          |            | -          |            |  71
|            | X          | -          | X          | 126
|            |            | -          | X          | 134
|            | X          | -          |            | 123

**Note**: `LOCK_MODE` was only tested once, because the different year data bundles are inserted concurrently in separate threads.

Based on those results the GitLab CI Proof Of Concept job `maven-poc-h2-url` is configured as `mvn org.owasp:dependency-check-maven:3.1.1:update-only -DconnectionString="jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;LOG=0;CACHE_SIZE=65536;"`.
The default [connectionString](https://jeremylong.github.io/DependencyCheck/dependency-check-maven/configuration.html) `jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;` is used in job `maven-poc-base:job`.

Alas, while the test runs on the local system were always around 1 minute versus 2 minutes, this reduction was a lot less on GitLab CI and sometimes the job performed slower.

## Database File Locking

Whenever a H2 Database is opened, a [lock file](https://www.h2database.com/html/features.html#database_file_locking) (in case of `dc.h2.db` a lock file `dc.lock.db`) is created to signal other processes that the database is in use.
If database is closed, or if the process that opened the database terminates, this lock file is deleted.
For more information about the algorithms, see [Advanced / File Locking Protocols](https://www.h2database.com/html/advanced.html#file_locking_protocols).

The following file locking methods are implemented:

 * The default method is `FILE` and uses a watchdog thread to protect the database file. The watchdog reads the lock file each second.
 * The second method is `SOCKET` and opens a server socket. The socket method does not require reading the lock file every second. *The socket method should only be used if the database files are only accessed by one (and always the same) computer.*
 * The third method is `FS`. This will use native file locking using FileChannel.lock.

And Dependency Check also adds its own lock file `dc.update.lock`:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/dependency-check/dependency-check-nist-nvd-h2-db.png)

**Note**: Aborting the Dependency Check Plugin can result in the following error "Existing update in progress":

![Existing update in progress](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/dependency-check/dependency-check-nist-nvd-h2-db-locked.png)

Manual clean up of local Maven repository `org/owasp/dependency-check-data/3.0` might be in order.

## Proof Of Concept Profiles

### Backup NVD Profile

The following profile `backupNvd` deploys a zip file containing the current NIST NVD to the release repository, that is configured with an `id` and an `url` in for example the parent POM  distributionManagement and a `server` entry in `settings.xml`.
A local hosted Nexus Repository in this case:

```xml
  <distributionManagement>
    <repository>
      <id>nexus</id>
      <name>Internal Releases</name>
      <url>${nexusHost}/content/repositories/releases</url>
    </repository>
  </distributionManagement>
```

All configurable stuff is placed in the following properties:

```xml
  <properties>
    <dependency.check.nvd.timestamp.format>yyyyMMdd.HHmmss</dependency.check.nvd.timestamp.format>
    <dependency.check.nvd.time.zone>America/New_York</dependency.check.nvd.time.zone>

    <dependency.check.nvd.version>3.0</dependency.check.nvd.version>
    <dependency.check.nvd.repository.directory>${settings.localRepository}/org/owasp/dependency-check-data/${dependency.check.nvd.version}</dependency.check.nvd.repository.directory>
    <dependency.check.nvd.name>dc.h2.db</dependency.check.nvd.name>

    <backup.archive.extension>zip</backup.archive.extension>
    <backup.archive.name>${dependency.check.nvd.name}.${backup.archive.extension}</backup.archive.name>
    <backup.archive.full.file.path>${dependency.check.nvd.repository.directory}/${backup.archive.name}</backup.archive.full.file.path>

    <backup.archive.group.id>nl.demon.shadowland.freedumbytes.maven.nvd.backup</backup.archive.group.id>
    <backup.archive.artifact.id>nist-nvd-cve</backup.archive.artifact.id>
    <backup.archive.classifier>db</backup.archive.classifier>
    <backup.archive.description>NVD Data Feeds CVE from https://nvd.nist.gov/vuln/data-feeds</backup.archive.description>
    <backup.archive.repository.id>${project.distributionManagement.repository.id}</backup.archive.repository.id>
    <backup.archive.repository.url>${project.distributionManagement.repository.url}</backup.archive.repository.url>
  <properties>
```

Some of those properties can be customized but this has to be done in the POM containing the profile, because of the way the properties in the parent POM are expanded before a child POM even comes in the picture.

Properties | Description | Current Value
-----------|-------------|--------------
dependency.check.nvd.timestamp.format | Timestamp of latest update | `yyyyMMdd.HHmmss`
dependency.check.nvd.time.zone | [Timezone](https://www.timeanddate.com/time/map/#!cities=4940) for [NVD Data Feeds](https://nvd.nist.gov/vuln/data-feeds) of [NIST](https://www.nist.gov/) location [Gaithersburg](https://www.timeanddate.com/worldclock/usa/gaithersburg) in Maryland | `America/New_York`
dependency.check.nvd.version | Dependency Check database version | `3.0`
dependency.check.nvd.repository.directory | Dependency Check database location | Local Maven repository `org/owasp/dependency-check-data/3.0`
dependency.check.nvd.name | Dependency Check database file name | `dc.h2.db`
backup.archive.extension | Archive file extension | `zip`
backup.archive.name | Archive file name | `dc.h2.db.zip`
backup.archive.full.file.path | Archive file path | Local Maven repository `nl/demon/shadowland/freedumbytes/maven/nvd/backup/nist-nvd-cve/3.0.20180225.083953/nist-nvd-cve-3.0.20180225.083953-db.zip`
backup.archive.group.id | Archive groupId | `nl.demon.shadowland.freedumbytes.maven.nvd.backup` possible alternative `org.owasp.dependency-check-backup`
backup.archive.artifact.id | Archive artifactId | `nist-nvd-cve`
backup.archive.classifier | Archive classifier | `db`
backup.archive.description | POM description | `NVD Data Feeds CVE from https://nvd.nist.gov/vuln/data-feeds`
backup.archive.repository.id | Remote Maven repository id | `project.distributionManagement.repository.id`
backup.archive.repository.url | Remote Maven repository url | `project.distributionManagement.repository.url`

And now for the profile itself:

```xml
  <profiles>
    <profile>
      <id>backupNvd</id>

      <build>
        <plugins>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>build-helper-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>backup-timestamp</id>
                <phase>validate</phase>
                <goals>
                  <goal>timestamp-property</goal>
                </goals>
                <configuration>
                  <name>backup.archive.date.time</name>
                  <pattern>${dependency.check.nvd.timestamp.format}</pattern>
                  <timeZone>${dependency.check.nvd.time.zone}</timeZone>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.owasp</groupId>
            <artifactId>dependency-check-maven</artifactId>
            <executions>
              <execution>
                <id>backup-update</id>
                <phase>generate-resources</phase>
                <goals>
                  <goal>update-only</goal>
                </goals>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>truezip-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>backup-archive</id>
                <phase>process-resources</phase>
                <goals>
                  <goal>copy</goal>
                </goals>
                <configuration>
                  <filesets>
                    <fileset>
                      <directory>${dependency.check.nvd.repository.directory}</directory>
                      <includes>
                        <include>${dependency.check.nvd.name}</include>
                      </includes>
                      <outputDirectory>${backup.archive.full.file.path}</outputDirectory>
                    </fileset>
                  </filesets>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-deploy-plugin</artifactId>
            <executions>
              <execution>
                <id>backup-deploy</id>
                <phase>process-resources</phase>
                <goals>
                  <goal>deploy-file</goal>
                </goals>
                <configuration>
                  <groupId>${backup.archive.group.id}</groupId>
                  <artifactId>${backup.archive.artifact.id}</artifactId>
                  <version>${dependency.check.nvd.version}.${backup.archive.date.time}</version>
                  <classifier>${backup.archive.classifier}</classifier>
                  <type>${backup.archive.extension}</type>
                  <description>${backup.archive.description}</description>
                  <file>${backup.archive.full.file.path}</file>
                  <repositoryId>${backup.archive.repository.id}</repositoryId>
                  <url>${backup.archive.repository.url}</url>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
```

Optionally remove the database with `mvn org.owasp:dependency-check-maven:purge` before creating a backup with `mvn process-resources -PbackupNvd`.

### Restore NVD Profile

The following profile `restoreNvd` unpacks a zip file containing a backup of the NIST NVD from [Maven Central](https://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22nl.demon.shadowland.freedumbytes.maven.nvd.backup%22%20AND%20a%3A%22nist-nvd-cve%22)
into the local Maven repository `org/owasp/dependency-check-data/3.0`.

For ease of use the configurable stuff from this profile is placed in properties:

```xml
  <properties>
    ...

    <restore.archive.date.time.default>20180225.083953</restore.archive.date.time.default>
    <restore.archive.version>${dependency.check.nvd.version}.${restore.archive.date.time.default}</restore.archive.version>
  <properties>
```

Properties | Description | Current Value
-----------|-------------|--------------
restore.archive.date.time.default | Timestamp of backup archive | `20180225.083953` from [Maven Central](https://search.maven.org/#artifactdetails%7Cnl.demon.shadowland.freedumbytes.maven.nvd.backup%7Cnist-nvd-cve%7C3.0.20180225.083953%7Czip)
restore.archive.version | Combination of Dependency Check database version and backup timestamp | `3.0.20180225.083953`

```xml
  <profiles>
    ...

    <profile>
      <id>restoreNvd</id>

      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <executions>
              <execution>
                <id>restore-backup</id>
                <phase>initialize</phase>
                <goals>
                  <goal>unpack</goal>
                </goals>
                <configuration>
                  <artifactItems>
                    <artifactItem>
                      <groupId>${backup.archive.group.id}</groupId>
                      <artifactId>${backup.archive.artifact.id}</artifactId>
                      <version>${restore.archive.version}</version>
                      <classifier>${backup.archive.classifier}</classifier>
                      <type>${backup.archive.extension}</type>
                      <overWrite>true</overWrite>
                      <outputDirectory>${dependency.check.nvd.repository.directory}</outputDirectory>
                    </artifactItem>
                  </artifactItems>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
```


Restore the archived database with `mvn initialize -PrestoreNvd`.

Although the first results looked promising, the real question is how often do we need to create a backup?
After just a week restoring the backup H2 Database and running `mvn org.owasp:dependency-check-maven:3.1.1:update-only -DconnectionString="..."` with default and fast import settings results in (comparison with the purge setup included):

H2 | Type | connectionString | Total  time (min:sec)
---|------|-----------------|---------------------:
Restore | Default | jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;| 3:57
Restore | Fast Database Import | jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;LOG=0;CACHE_SIZE=65536;| 1:25
Purge | Default | jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;| 2:04
Purge | Fast Database Import | jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;LOG=0;CACHE_SIZE=65536;| 1:01


### Open Source NVD Profile

As a reminder and to complete the story how the following profile `openSourceNvd` was used to (semi-)[Manually Deploying to OSSRH](https://central.sonatype.org/pages/manual-staging-bundle-creation-and-deployment.html):

```xml
  <properties>
    ...

    <open.source.build.directory>${project.build.directory}/open-source</open.source.build.directory>
    <open.source.build.name>db.zip</open.source.build.name>
    <open.source.repository.id>ossrh</open.source.repository.id>
    <open.source.repository.url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</open.source.repository.url>

    <com.google.code.maven.replacer.plugin.version>1.5.3</com.google.code.maven.replacer.plugin.version>
  <properties>
```

```xml
  <profiles>
    ...

    <profile>
      <id>openSourceNvd</id>

      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <executions>
              <execution>
                <id>copy-backup</id>
                <phase>initialize</phase>
                <goals>
                  <goal>copy</goal>
                </goals>
                <configuration>
                  <artifactItems>
                    <artifactItem>
                      <groupId>${backup.archive.group.id}</groupId>
                      <artifactId>${backup.archive.artifact.id}</artifactId>
                      <version>${restore.archive.version}</version>
                      <classifier>${backup.archive.classifier}</classifier>
                      <type>${backup.archive.extension}</type>
                      <overWrite>true</overWrite>
                      <outputDirectory>${open.source.build.directory}</outputDirectory>
                      <destFileName>${open.source.build.name}</destFileName>
                    </artifactItem>
                  </artifactItems>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>com.google.code.maven-replacer-plugin</groupId>
            <artifactId>replacer</artifactId>
            <version>${com.google.code.maven.replacer.plugin.version}</version>
            <executions>
              <execution>
                <id>copy-pom</id>
                <phase>initialize</phase>
                <goals>
                  <goal>replace</goal>
                </goals>
                <configuration>
                  <basedir>${project.basedir}/src/templates</basedir>
                  <outputDir>../../target/open-source</outputDir>
                  <includes>
                    <include>pom.xml</include>
                  </includes>
                  <replacements>
                    <replacement>
                      <token>\$\{restore.archive.version\}</token>
                      <value>${restore.archive.version}</value>
                    </replacement>
                    <replacement>
                      <token>\$\{projectRoot\}</token>
                      <value>${projectRoot}</value>
                    </replacement>
                    <replacement>
                      <token>\$\{sourceConnection\}</token>
                      <value>${sourceConnection}</value>
                    </replacement>
                    <replacement>
                      <token>\$\{sourceDevConnection\}</token>
                      <value>${sourceDevConnection}</value>
                    </replacement>
                    <replacement>
                      <token>\$\{sourceWebRoot\}</token>
                      <value>${sourceWebRoot}</value>
                    </replacement>
                    <replacement>
                      <token>\$\{organizationName\}</token>
                      <value>${organizationName}</value>
                    </replacement>
                    <replacement>
                      <token>\$\{organizationHost\}</token>
                      <value>${organizationHost}</value>
                    </replacement>
                    <replacement>
                      <token>\$\{mavenHost\}</token>
                      <value>${mavenHost}</value>
                    </replacement>
                  </replacements>
                </configuration>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-gpg-plugin</artifactId>
            <executions>
              <execution>
                <id>sign-backup</id>
                <phase>process-resources</phase>
                <goals>
                  <goal>sign-and-deploy-file</goal>
                </goals>
                <configuration>
                  <keyname>${gpg.keyname}</keyname>
                  <passphraseServerId>${gpg.keyname}</passphraseServerId>
                  <gpgArguments>
                    <arg>--pinentry-mode</arg>
                    <arg>loopback</arg>
                  </gpgArguments>
                  <repositoryId>${open.source.repository.id}</repositoryId>
                  <url>${open.source.repository.url}</url>
                  <pomFile>${open.source.build.directory}/pom.xml</pomFile>
                  <file>${open.source.build.directory}/${open.source.build.name}</file>
                  <classifier>${backup.archive.classifier}</classifier>
                  <packaging>${backup.archive.extension}</packaging>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
```

Stage the archived database with `mvn process-resources -PopenSourceNvd` and [Releasing Deployment from OSSRH to the Central Repository](https://central.sonatype.org/pages/releasing-the-deployment.html).

Filter in Staging Reporsitory on package and select the bundle:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/nexus/ossrh-staging-repositories-summary.png)

View the bundle content:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/nexus/ossrh-staging-repositories-content.png)

View the bundle activity:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/nexus/ossrh-staging-repositories-activity-created.png)

Click `Close` button:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/nexus/ossrh-staging-repositories-activity-close-confirm.png)

View the bundle evaluation activity:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/nexus/ossrh-staging-repositories-activity-closed.png)

Click `Release` button:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/nexus/ossrh-staging-repositories-activity-release-confirm.png)

View the bundle release activity:

![Lock files](https://freedumbytes.gitlab.io/dependency-check-nist-nvd/images/nexus/ossrh-staging-repositories-activity-releasing.png)


# Summary

Performance measurements on GitLab CI isn't really possible. Thus the final conclusion is based on local mvn build runs.
Perhaps the way in which H2 Database updates are applied, causes the database updates, after just a week of inactivity, to take double the amount of time (4 min) versus creating it from scratch (after purge 2 min).
Changing the default H2 Database connection setting (`jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;`) with `-DconnectionString="jdbc:h2:file:%s;MV_STORE=FALSE;AUTOCOMMIT=ON;LOG=0;CACHE_SIZE=65536;"` for H2 Fast Database Import settings,
reduces the build time for `update-only` to 58 sec (after purge) and 1:25 min (for update after just a week of inactivity).
